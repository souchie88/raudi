const Utilisateur = require('../models/utilisateurModel')
const Modele = require('../models/modeleModel')
const Option = require('../models/optionModel')
const Historique = require('../models/historiqueModel')
const sequelize = require('../database/database')

exports.createTableUtilisateur = async(req, res)=>{
    await Utilisateur.sync()
    res.status(200).json('Table utilisateur créé')
}
exports.createTableModele = async(req, res)=>{
    await Modele.sync()
    res.status(200).json('Table modèle créé')
}

exports.createTableOption = async(req, res)=>{
    await Option.sync()
    res.status(200).json('Table option créé')   
}

exports.createTableHistorique = async(req, res)=>{
    await Historique.sync()
    res.status(200).json('Table historique créé')
}

exports.createAllTable = async(req, res)=>{
    await sequelize.sync()
    res.status(200).json('Toutes les tables sont créés')
}