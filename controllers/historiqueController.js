const Historique = require('../models/historiqueModel');
const sequelize = require('../database/database');

// Fonction pour afficher tout l'historique
exports.AllHistorique = async (req, res) => {
    try {
        // Récupération de tous les enregistrements historiques depuis la base de données
        const historiques = await Historique.findAll();
        
        // Envoi des données historiques en tant que réponse avec le statut HTTP 200 (OK)
        res.status(200).json(historiques);
    } catch (erreur) {
        // En cas d'erreur, affichage de l'erreur dans la console et envoi d'une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la récupération de l\'historique' });
    }
}

// Fonction pour afficher le total des prix
exports.TotalPrix = async (req, res) => {
    try {
        // Calcul du total des prix en faisant la somme de la colonne 'prixModele' dans la table Historique
        const totalPrix = await Historique.sum('prixModele');
        
        // Envoi du total des prix en tant que réponse avec le statut HTTP 200 (OK)
        res.status(200).json({ totalPrix });
    } catch (erreur) {
        // En cas d'erreur, affichage de l'erreur dans la console et envoi d'une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors du calcul du total des prix' });
    }
};
