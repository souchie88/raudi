const Modele = require('../models/modeleModel')
const sequelize = require('../database/database')

// Crée un modèle
exports.CreateModele = async (req, res) => {
    try {
        // Récupère les données du modèle depuis le corps de la requête
        let modele = req.body;

        // Crée un nouvel enregistrement de modèle dans la base de données
        await Modele.create(modele);

        // Envoi d'une réponse avec le statut HTTP 201 (Created) indiquant que le modèle a été créé avec succès
        res.status(201).json('Modèle bien créé');
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la création du modèle' });
    }
};

// Modifie un modèle
exports.UpdateModele = async (req, res) => {
    const modeleId = req.params.id;

    try {
        // Recherche le modèle dans la base de données par son identifiant
        const modele = await Modele.findByPk(modeleId);

        if (modele) {
            // Met à jour le modèle avec les données fournies dans le corps de la requête
            await modele.update(req.body);
            
            // Envoi d'une réponse avec le statut HTTP 200 (OK) indiquant que le modèle a été mis à jour avec succès
            res.status(200).json('Modèle bien mis à jour');
        } else {
            // Si le modèle n'est pas trouvé, envoi d'une réponse avec le statut HTTP 404 (Not Found)
            res.status(404).json({ erreur: 'Modèle non trouvé' });
        }
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la mise à jour du modèle' });
    }
};

// Affiche tous les modèles
exports.AllModele = async (req, res) => {
    try {
        // Récupère tous les modèles depuis la base de données
        const modeles = await Modele.findAll();
        
        // Envoi des modèles en tant que réponse avec le statut HTTP 200 (OK)
        res.status(200).json(modeles);
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la récupération des modèles' });
    }
};

// Affiche un seul modèle
exports.MdlModele = async (req, res) => {
    const modeleId = req.params.id;

    try {
        // Recherche le modèle dans la base de données par son identifiant
        const modele = await Modele.findByPk(modeleId);

        if (modele) {
            // Envoi du modèle en tant que réponse avec le statut HTTP 200 (OK)
            res.status(200).json(modele);
        } else {
            // Si le modèle n'est pas trouvé, envoi d'une réponse avec le statut HTTP 404 (Not Found)
            res.status(404).json({ erreur: 'Modèle non trouvé' });
        }
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la récupération du modèle' });
    }
};

// Supprime un modèle
exports.DeleteModele = async (req, res) => {
    const modeleId = req.params.id;

    try {
        // Recherche le modèle dans la base de données par son identifiant
        const modele = await Modele.findByPk(modeleId);

        if (modele) {
            // Supprime le modèle de la base de données
            await modele.destroy();

            // Envoi d'une réponse avec le statut HTTP 204 (No Content) indiquant que le modèle a été supprimé avec succès
            res.status(204).json('Modèle bien supprimé');
        } else {
            // Si le modèle n'est pas trouvé, envoi d'une réponse avec le statut HTTP 404 (Not Found)
            res.status(404).json({ erreur: 'Modèle non trouvé' });
        }
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la suppression du modèle' });
    }
};
