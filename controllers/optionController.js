const Option = require('../models/optionModel')
const Historique = require('../models/historiqueModel');
const sequelize = require('../database/database')

// Création d'une option
exports.CreateOption = async (req, res) => {
    try {
        // Récupère les données de l'option depuis le corps de la requête
        let option = req.body;

        // Crée un nouvel enregistrement d'option dans la base de données
        await Option.create(option);

        // Récupère les informations nécessaires pour l'historique
        const { idModele, prixModele } = option;

        // Crée une entrée dans la table Historique pour enregistrer l'historique de l'option
        await Historique.create({
            nomModele: idModele,
            prixModele: prixModele,
            emailUtilisateur: 'email@exemple.com',  // Remplacez par l'email réel si disponible
            dateAchat: new Date(),
        });

        // Envoi d'une réponse avec le statut HTTP 201 (Created) indiquant que l'option a été créée avec succès
        res.status(201).json('Option bien créée');
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la création de l\'option' });
    }
};

// Affiche toutes les options
exports.AllOptions = async (req, res) => {
    try {
        // Récupère toutes les options depuis la base de données
        const options = await Option.findAll();
        
        // Envoi des options en tant que réponse avec le statut HTTP 200 (OK)
        res.status(200).json(options);
    } catch (erreur) {
        // En cas d'erreur, affiche l'erreur dans la console et envoie une réponse avec le statut HTTP 500 (Internal Server Error)
        console.error(erreur);
        res.status(500).json({ erreur: 'Erreur lors de la récupération des options' });
    }
};
