const Utilisateur = require('../models/utilisateurModel');
const jwt = require('jsonwebtoken');
require('dotenv').config();

// Connexion
exports.Login = async (req, res) => {
  try {
    // Récupérer l'email et le mot de passe de la requête
    const { email, password } = req.body;

    // Rechercher l'utilisateur par email dans la base de données
    const utilisateur = await Utilisateur.findOne({
      where: {
        email: email
      }
    });

    // Vérifier si l'utilisateur existe
    if (!utilisateur) {
      // Si l'utilisateur n'existe pas, renvoie une réponse avec le statut HTTP 401 (Unauthorized)
      return res.status(401).json({ error: 'Utilisateur non existant' });
    }

    // Comparaison du mot de passe en texte brut
    if (password !== utilisateur.password) {
      // Si le mot de passe ne correspond pas, renvoie une réponse avec le statut HTTP 401 (Unauthorized)
      return res.status(401).json({ error: 'Mot de passe incorrect' });
    }

    // Générer un jeton JWT pour l'utilisateur avec une clé secrète et une expiration d'une heure
    const token = jwt.sign({ email }, process.env.SECRET_KEY, { expiresIn: '1h' });

    // Renvoyer le jeton JWT en réponse
    res.json({ token });
  } catch (error) {
    // En cas d'erreur, affiche l'erreur dans la console et renvoie une réponse avec le statut HTTP 500 (Internal Server Error)
    console.error(error);
    res.status(500).json({ error: 'Erreur serveur' });
  }
};
