const Sequelize = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize(process.env.DBDATABASE, process.env.DBUSER, process.env.DBPASSWORD, {
    host: process.env.DBHOST,
    dialect: process.env.DBDIALECT
});

sequelize.authenticate().then(() => {
    console.log('Authentification à la base de données réussie');
}).catch((err) => {
    console.error('Erreur d\'authentification à la base de données:', err);
});

module.exports = sequelize;
