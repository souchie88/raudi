/*const jwt = require('jsonwebtoken');
require('dotenv').config();

// Middleware d'authentification avec token JWT
exports.authenticator = (req, res, next) => {
  try {
    // Récupérer le token depuis les paramètres de l'URL ou les en-têtes de la requête
    const token = req.params.token || req.headers.authorization;

    // Vérifier si un token et une clé secrète sont présents
    if (token && process.env.SECRET_KEY) {
      // Vérifier le token en utilisant la clé secrète
      jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
        // Si une erreur survient lors de la vérification du token
        if (err) {
          // Affiche l'erreur dans la console et renvoie une réponse avec le statut HTTP 401 (Unauthorized)
          console.error(err);
          return res.status(401).json({ erreur: 'Accès refusé' });
        }

        // Si la vérification est réussie, ajouter les données décodées à la requête
        req.decoded = decoded;
        // Appeler la fonction suivante dans la chaîne middleware (le gestionnaire de route suivant)
        next();
      });
    } else {
      // Si aucun token ou clé secrète n'est présent, renvoie une réponse avec le statut HTTP 401 (Unauthorized)
      res.status(401).json({ erreur: 'Accès refusé' });
    }
  } catch (error) {
    // En cas d'erreur, affiche l'erreur dans la console et renvoie une réponse avec le statut HTTP 500 (Internal Server Error)
    console.error(error);
    res.status(500).json({ erreur: 'Erreur serveur' });
  }
};*/

const jwt = require('jsonwebtoken');
require('dotenv').config();

// Middleware d'authentification avec vérification du rôle
exports.authenticator = (req, res, next) => {
  try {
    // Récupérer le token depuis les paramètres de l'URL ou les en-têtes de la requête
    const token = req.params.token || req.headers.authorization;

    // Vérifier si un token et une clé secrète sont présents
    if (token && process.env.SECRET_KEY) {
      // Vérifier le token en utilisant la clé secrète
      jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
        // Si une erreur survient lors de la vérification du token
        if (err) {
          // Affiche l'erreur dans la console et renvoie une réponse avec le statut HTTP 401 (Unauthorized)
          console.error(err);
          return res.status(401).json({ erreur: 'Accès refusé' });
        }

        // Ajouter les données décodées à la requête
        req.decoded = decoded;

        // Vérifier le rôle de l'utilisateur
        const { role } = decoded;
        if (role !== 'admin' && role !== 'client' && role !== 'comptable') {
          // Si le rôle n'est pas valide, renvoie une réponse avec le statut HTTP 403 (Forbidden)
          return res.status(403).json({ erreur: 'Accès refusé. Rôle non autorisé.' });
        }

        // Appeler la fonction suivante dans la chaîne middleware (le gestionnaire de route suivant)
        next();
      });
    } else {
      // Si aucun token ou clé secrète n'est présent, renvoie une réponse avec le statut HTTP 401 (Unauthorized)
      res.status(401).json({ erreur: 'Accès refusé' });
    }
  } catch (error) {
    // En cas d'erreur, affiche l'erreur dans la console et renvoie une réponse avec le statut HTTP 500 (Internal Server Error)
    console.error(error);
    res.status(500).json({ erreur: 'Erreur serveur' });
  }
};

// Fonction pour créer un token JWT avec le rôle de l'utilisateur
exports.createToken = (email, role) => {
  const token = jwt.sign({ email, role }, process.env.SECRET_KEY, { expiresIn: '1h' });
  return token;
};
