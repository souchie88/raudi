const sequelize = require('../database/database');
const { DataTypes } = require('sequelize');

const Historique = sequelize.define('historique', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nomModele: {
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    prixModele: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    emailUtilisateur: {
        type: DataTypes.STRING(255),
        allowNull: false,
        validate: {
            isEmail: true,
        }
    },
    dateAchat: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize,
    freezeTableName: true
});

module.exports = Historique;
