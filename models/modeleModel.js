const sequelize = require('../database/database');
const { DataTypes } = require('sequelize');

const Modele = sequelize.define('modele', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nom: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: true
    },
    portes: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    motorisation: {
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    couleur: {
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    vitesse: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    prix: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
}, {
    sequelize,
    freezeTableName: true
});

module.exports = Modele;
