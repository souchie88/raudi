const sequelize = require('../database/database');
const { DataTypes } = require('sequelize');

const Modele = require('./modeleModel');

const Option = sequelize.define('option', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    clim: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
    },
    phares_xenon: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
    },
    siege_chauffant: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
    },
    interieur_cuir: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
    },
    prix: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    idModele: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'modele',
            key: 'id'
        }
    }
}, {
    sequelize,
    freezeTableName: true
});

Option.belongsTo(Modele, { foreignKey: 'idModele', as: 'modele' });

module.exports = Option;
