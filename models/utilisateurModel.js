const sequelize = require('../database/database');
const { DataTypes } = require('sequelize');

const Utilisateur = sequelize.define('utilisateur', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    prenom: {
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    nom: {
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true,
        }
    },
    password: {
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    role: {
        type: DataTypes.STRING(255),
        allowNull: false,
    }
}, {
    sequelize,
    freezeTableName: true
});

module.exports = Utilisateur;