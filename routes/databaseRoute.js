const express = require('express')
const route = express.Router()
const databaseController = require('../controllers/databaseController')

route.get('/createTableUtilisateur', databaseController.createTableUtilisateur)
route.get('/createTableModele', databaseController.createTableModele)
route.get('/createTableOption', databaseController.createTableOption)
route.get('/createTableHistorique', databaseController.createTableHistorique)

module.exports = route