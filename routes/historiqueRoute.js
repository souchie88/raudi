const express =require('express')
const sequelize = require('../database/database')
const route = express.Router()
const historiqueController = require('../controllers/historiqueController')

route.get('/all', historiqueController.AllHistorique)
route.get('/TotalPrix', historiqueController.TotalPrix);

module.exports = route