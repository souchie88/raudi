const express =require('express')
const sequelize = require('../database/database')
const route = express.Router()
const modeleController = require('../controllers/modeleController')

route.post('/create', modeleController.CreateModele)
route.put('/update/:id', modeleController.UpdateModele)
route.get('/all', modeleController.AllModele)
route.get('/mdl/:id', modeleController.MdlModele)
route.delete('/delete/:id', modeleController.DeleteModele)

module.exports = route