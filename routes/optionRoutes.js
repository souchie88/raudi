const express =require('express')
const sequelize = require('../database/database')
const route = express.Router()
const optionController = require('../controllers/optionController')

route.post('/create', optionController.CreateOption)
route.post('/all', optionController.AllOptions)

module.exports = route