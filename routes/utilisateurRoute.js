/*const express = require('express')
const middleware = require('../middleware/middleware')
const route = express.Router()
const utilisateurController = require('../controllers/utilisateurController')

route.post('/login', utilisateurController.Login, middleware.authenticator);

module.exports = route*/
const express = require('express');
const middleware = require('../middleware/middleware');
const route = express.Router();
const utilisateurController = require('../controllers/utilisateurController');

// Route pour la connexion (login)
route.post('/login', (req, res, next) => {
  // Utilisez le contrôleur pour la connexion
  utilisateurController.Login(req, res, (err, user) => {
    if (err) {
      return res.status(401).json({ error: err.message });
    }

    // Créer un jeton JWT avec le rôle de l'utilisateur
    const token = middleware.createToken(user.email, user.role);

    // Envoyer le jeton en réponse
    res.json({ token });
  });
});

// Routes spécifiques pour l'administration (role: 'admin')
route.get('/admin/dashboard', middleware.authenticator, (req, res) => {
  // Logique de gestion pour la partie admin
  res.render('admin/dashboard');
});

// Routes spécifiques pour les clients (role: 'client')
route.get('/client/home', middleware.authenticator, (req, res) => {
  // Logique de gestion pour la partie client
  res.render('client/home');
});

// Routes spécifiques pour les comptables (role: 'comptable')
route.get('/comptable/home', middleware.authenticator, (req, res) => {
  // Logique de gestion pour la partie comptable
  res.render('comptable/home');
});

module.exports = route;
