const express = require('express')
const cors = require('cors')

const databaseRoute = require('./routes/databaseRoute')
const utilisateurRoute = require('./routes/utilisateurRoute')
const modeleRoute = require('./routes/modeleRoute')
const optionRoute = require('./routes/optionRoutes')
const historiqueRoute = require('./routes/historiqueRoute')

const app = express()

app.use(express.json())
app.use(cors())

app.use('/database', databaseRoute)
app.use('/utilisateur', utilisateurRoute)
app.use('/modele', modeleRoute)
app.use('/option', optionRoute)
app.use('/historique', historiqueRoute)

app.listen(8000, ()=>{
    console.log("serveur lancé sur le port 8000")
})

app.get('/admin', (req, res) => {
    res.sendFile(__dirname + '/public/admin.html');
});

app.get('/home', (req, res) => {
    res.sendFile(__dirname + '/public/home.html');
});

app.get('/connexion', (req, res) => {
    res.sendFile(__dirname + '/public/connexion.html');
});

app.get('/historique', (req, res) => {
    res.sendFile(__dirname + '/public/historique.html');
});